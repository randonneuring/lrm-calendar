/*global $*/
'use strict';


// External libraries
var Plotly, DATA_LOAD,

    // Global variables
    DATA_DISPLAY =
    {
        plotCanvas : {
            "0" : document.getElementById('plotCanvas0'),
            "1" : document.getElementById('plotCanvas1'),
            "2" : document.getElementById('plotCanvas2'),
        },
        plotBoolean : {
            "year" : {
                "ALL" : false,
            },
            "distance" : {
                "ALL" : false,
            },
            "place" : {
                "ALL" : true
            },
            "date" : {
                "ALL" : true
            },
        },
        plotData : {
            "xScatter" : [],
            "yScatter" : [],
            "xHist" : [],
            "numParticpants" : [],
            "hoverText" : [],
            "opacity" : [],
            "markerSize" : [],
            "annotations" : {
                "0" : [],
                "1" : [],
                "2" : [],
            }
        },

        // Different colors for different years
        // Good color options:
        //   https://colorbrewer2.org/
        //      #type=qualitative&scheme=Dark2&n=3
        plotSettings : {
            "color" : {
                "2021" : "#b1e0d2",
                "2020" : "#377eb8",
                "2019" : "#e41a1c",
                "2018" : "#1b9e77",
                "2017" : "#7570b3",
                "2016" : "#ffff33",
                "2015" : "#ff7f00",
                "2014" : "#80b1d3",
                "2013" : "#f781bf",
                "2012" : "#b3de69",
                "2011" : "#bebada",
                "2010" : "#ffffb3",
                "2009" : "#fdb462",
                "2008" : "#b3cde3",
                "2007" : "#fddaec",
                "2006" : "#ccebc5",
                "2005" : "#decbe4",
                "2004" : "#ffffcc",
                "2003" : "#fed9a6",
            },
        },
        dataTraces : {
            "0" : [],
            "1" : [],
            "2" : [],
        },
        // for menu creations
        startDateList : {},
        // Set some options - mostly hide all the plotly buttons
        options : {
            "staticPlot" : false,
            "showLink" : false,
            "displaylogo" : false,
            "modeBarButtonsToRemove" : [
                'sendDataToCloud', 'hoverCompareCartesian',
                'hoverClosestCartesian', "resetScale2d",
                'hoverClosest3d', 'resetCameraLastSave3d', "toImage",
                'orbitRotation', "zoom2d", 'zoomIn2d', 'zoomOut2d',
                "pan2d", "toggleSpikelines", "lasso2d", "select2d"],
            "displayModeBar" : true,
            "showTips" : false,
            "scrollZoom" : true,
            "responsive" : true,
        },
        zoomRange : {
            "selected" : {
                "time" : [0, 300],
                "distance" : [0, 3000],
                "numPeople" : [0, 300],
            },
            "default" : {
                "time" : [0, 300],
                "distance" : [0, 3000],
                "numPeople" : [0, 300],
            }
        },


        fillDataTraces : function (traceArrayNumber, type, xDataArrays,
            yDataArrays, binRange, binSize) {

            var debug = false, dataTrace = {};

            if (debug) {
                console.log('DATA_DISPLAY fillDataTraces called');
            }

            // Create histogram traces
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {
                dataTrace = {
                    "name" : year,
                    "type" : type,
                    "visible" : true,

                    "x" : xDataArrays[year],

                    "marker" : {
                        "color" : DATA_DISPLAY.plotSettings.color[year],
                    },

                    "autobinx" : false,
                };

                if (type === "scattergl") {
                    dataTrace.mode = "markers";
                    dataTrace.y = yDataArrays[year];
                    dataTrace.marker.opacity =
                        DATA_DISPLAY.plotData.opacity[year];
                    dataTrace.marker.size =
                        DATA_DISPLAY.plotData.markerSize[year];
                    dataTrace.marker.line = {"width" : 1};
                    dataTrace.hoverinfo = "text";
                    dataTrace.hovertext =
                        DATA_DISPLAY.plotData.hoverText[year];
                }

                if (type === "histogram") {
                    dataTrace.hoverinfo = "all";
                    dataTrace.marker.opacity = 0.5;
                    dataTrace.xbins = {};
                    dataTrace.xbins.start = binRange[0];
                    dataTrace.xbins.end = binRange[1];
                    dataTrace.xbins.size = binSize;
                }

                DATA_DISPLAY.dataTraces[traceArrayNumber].push(dataTrace);
            });

            if (debug) {
                console.log(DATA_DISPLAY.dataTraces[traceArrayNumber]);
            }
        },


        drawPlots : function (traceArrayNumber, xRange, yRangeFixed, mainTitle,
            xTitle, yTitle, annotations) {

            var debug = false, layout;

            if (debug) {
                console.log('DATA_DISPLAY drawPlots called');
            }

            // And the layout - titles, grids, etc.
            layout = {
                "title" : "<b>" + mainTitle + "</b>",

                "xaxis" : {
                    "title" : "<b>" + xTitle + "</b>",
                    "showgrid" : true,
                    "zeroline" : false,
                    "titlefont" : {"color" : "#DDDDCE"},
                    "tickfont" : {"color" : "#999"},
                    "autorange" : true,
                    "range" : xRange,
                },

                "yaxis" : {
                    "title" : "<b>" + yTitle + "</b>",
                    "showgrid" : true,
                    "zeroline" : false,
                    "titlefont" : {"color" : "#DDDDCE"},
                    "tickfont" : {"color" : "#999"},
                    "fixedrange" : yRangeFixed,
                },

                "annotations": annotations,
                "showlegend" : false,
                "margin" : { l: 65, r: 30, b: 70, t: 50, },
                "hovermode" : "x",
                "bargap" : 0,
                "barmode" : "overlay",
                "titlefont" : {"color" : "#DDDDCE"},
                "paper_bgcolor" : '#212021',
                "plot_bgcolor" : '#212021',
            };

            // Display the plot in the html div specified
            Plotly.newPlot(DATA_DISPLAY.plotCanvas[traceArrayNumber],
                DATA_DISPLAY.dataTraces[traceArrayNumber],
                layout,
                DATA_DISPLAY.options);

            // Refill the profile projections when a zoom event occurs
            // Why isn't this properly done already in the plotly library?!
            DATA_DISPLAY.plotCanvas[traceArrayNumber].on('plotly_relayout',
                function (eventdata) {

                    if (debug) {
                        console.log('DATA_DISPLAY.plotCanvas[' +
                            traceArrayNumber + '] plotly_relayout ');
                        console.log(eventdata);
                    }

                    DATA_DISPLAY.handleZoom(traceArrayNumber, eventdata);
                });
        },


        // Remove or add data points with a given distance from the
        // plotted data
        toggleItem : function (key, value) {

            var debug = false, name, allChecked = true;

            // Make sure the input are set to strings
            value = String(value);
            key = String(key);

            if (debug) {
                console.log('toggleItem called - key: ', key, ' value: ',
                    value);
            }

            // Take care of the ALL cases first
            // If ALL has been clicked, than all of the items in that option
            // type will get the same visibility as ALL, i.e. all will be set
            // to true or false
            if (value === "ALL") {
                DATA_DISPLAY.plotBoolean[key][value] =
                    !DATA_DISPLAY.plotBoolean[key][value];
            } else {
                DATA_DISPLAY.plotBoolean[key].ALL = false;
            }

            // Toggle the ALL check mark in the html
            name = '#plotOption' + key + 'ALL' + ' .fa-check';
            if (DATA_DISPLAY.plotBoolean[key].ALL) {
                $(name).css('visibility', 'visible');
            } else {
                $(name).css('visibility', 'hidden');
            }

            // Loop over all item key values in reverse order
            // (e.g. for year, loop 2020, 2019, 2018, etc.)
            Object.keys(
                DATA_DISPLAY.plotBoolean[key]
            ).slice().reverse().forEach(
                function (itemValue) {

                    var j, dateOption;

                    // ALL has already been checked
                    if (itemValue === "ALL") {
                        return;
                    }

                    if (debug) {
                        console.log("itemValue: ", itemValue);
                    }

                    // If this is the special option value of ALL, toggle all
                    // options within the same type.
                    if (value === "ALL" || value === itemValue) {

                        // Toggle whether or not this item is to be displayed
                        if (value === "ALL") {
                            DATA_DISPLAY.plotBoolean[key][itemValue] =
                                DATA_DISPLAY.plotBoolean[key].ALL;
                        } else {
                            DATA_DISPLAY.plotBoolean[key][itemValue] =
                                !DATA_DISPLAY.plotBoolean[key][itemValue];
                        }

                        // Toggle check mark in the html
                        name = '#plotOption' + key + itemValue + ' .fa-check';
                        if (DATA_DISPLAY.plotBoolean[key][itemValue]) {
                            $(name).css('visibility', 'visible');
                        } else {
                            $(name).css('visibility', 'hidden');
                        }

                        // Populate the start date option list if a year option
                        // has been clicked
                        if (key === "year") {
                            if (debug) {
                                console.log(itemValue);
                            }

                            // First set all individual date options to true,
                            // this allows plotting to occur while the slow
                            // process of updating the html takes place
                            for (j = 0; j <
                                    DATA_DISPLAY.startDateList[
                                        itemValue
                                    ].length;
                                    j += 1) {
                                // Add to global variable, set visibility
                                dateOption =
                                    DATA_DISPLAY.startDateList[itemValue][j];
                                DATA_DISPLAY.plotBoolean.date[dateOption] =
                                    DATA_DISPLAY.plotBoolean.year[itemValue];
                            }

                            // Update the html, delay a bit so it does not
                            // interrupt the UI too much
                            setTimeout(function () {
                                if (debug) {
                                    console.log(itemValue);
                                }

                                DATA_DISPLAY.populateHTMLDateList(itemValue);
                            }, 20);

                        }

                    }

                    // Note if all item values are true or false
                    if (!DATA_DISPLAY.plotBoolean[key][itemValue]) {
                        allChecked = false;
                    }
                }
            );

            if (debug) {
                console.log("allChecked: ", allChecked);
            }

            // If all have been selected without actually clicking the ALL
            // option (meaning everything in the list has been clicked, in
            // effect doing the same as clicking ALL), update ALL to true
            // NOTE: this is kinda ugly, should re-do this entire function...
            if (allChecked) {
                DATA_DISPLAY.plotBoolean[key].ALL = true;

                // Toggle the ALL check mark in the html
                name = '#plotOption' + key + 'ALL' + ' .fa-check';
                if (DATA_DISPLAY.plotBoolean[key].ALL) {
                    $(name).css('visibility', 'visible');
                } else {
                    $(name).css('visibility', 'hidden');
                }
            }

            DATA_DISPLAY.reDrawPlots();
        },


        reDrawPlots : function () {

            // Decide which data to display or not, depending on what has
            // been selected in the option lists or by zooming
            DATA_DISPLAY.filterData();

            // Calculate simple stuff like total number and average in the plot
            DATA_DISPLAY.calculateStatistics();

            // Execute the plotly update command for all of the canvases
            DATA_DISPLAY.updatePlots();
        },


        updatePlots : function () {

            DATA_DISPLAY.updatePlot("0",
                DATA_DISPLAY.zoomRange.selected.time,
                "Tid (timmar)", "scattergl",
                DATA_DISPLAY.plotData.xScatter,
                DATA_DISPLAY.plotData.yScatter,
                [], 1,
                DATA_DISPLAY.plotData.annotations["0"]);

            DATA_DISPLAY.updatePlot("1",
                DATA_DISPLAY.zoomRange.selected.time,
                "Tid (timmar)", "histogram",
                DATA_DISPLAY.plotData.xHist, false,
                DATA_DISPLAY.zoomRange.default.time, 0.25,
                DATA_DISPLAY.plotData.annotations["1"]);

            DATA_DISPLAY.updatePlot("2",
                DATA_DISPLAY.zoomRange.selected.numPeople,
                "Nummer av Deltagare", "histogram",
                DATA_DISPLAY.plotData.numParticpants, false,
                DATA_DISPLAY.zoomRange.default.time, 4,
                DATA_DISPLAY.plotData.annotations["2"]);
        },


        updatePlot : function (traceArrayNumber, xRange, xTitle, type,
            xDataArrays, yDataArrays, binRange, binSize, annotations) {

            var debug = false, visibleArray = [], xArray = [], yArray = [],
                hovertextArray = [], markerArray = [], xbinsArray = [],
                count = 0, countArray = [];

            // All of the arrays of values must be placed into arrays, which
            // are in the same order as the traces are listed below.
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {
                visibleArray.push(DATA_DISPLAY.plotBoolean.year[year]);
                xArray.push(xDataArrays[year]);

                if (type === "scattergl") {
                    yArray.push(yDataArrays[year]);
                    xbinsArray.push({});
                    markerArray.push(
                        {
                            "color" : DATA_DISPLAY.plotSettings.color[year],
                            "opacity" : DATA_DISPLAY.plotData.opacity[year],
                            "size" : DATA_DISPLAY.plotData.markerSize[year],
                            "line" : {"width" : 1}
                        }
                    );
                    hovertextArray.push(DATA_DISPLAY.plotData.hoverText[year]);
                }
                if (type === "histogram") {
                    xbinsArray.push(
                        {
                            "start" : binRange[0],
                            "end" : binRange[1],
                            "size" : binSize,
                        }
                    );
                    markerArray.push(
                        {
                            "color" : DATA_DISPLAY.plotSettings.color[year],
                            "opacity" : 0.5,
                        }
                    );
                }

                countArray.push(count);
                count = count + 1;
            });

            // Redraw plot with filtered data arrays.
            Plotly.update(
                DATA_DISPLAY.plotCanvas[traceArrayNumber],
                {
                    "visible" : visibleArray,
                    "x" : xArray,
                    "y" : yArray,
                    "hovertext" : hovertextArray,
                    "marker" : markerArray,
                    "xbins" : xbinsArray,
                },
                {
                    "xaxis" : {
                        "title" : "<b>" + xTitle + "</b>",
                        "showgrid" : true,
                        "zeroline" : false,
                        "titlefont" : {"color" : "#DDDDCE"},
                        "tickfont" : {"color" : "#999"},
                        "autorange" : true,
                        "range" : xRange,
                    },
                    "annotations": annotations,
                },
                // Here are the numbers of the traces, and the order in which
                // the above data is set into them.
                countArray
            );

            if (debug) {
                console.log("Done with updatePlot");
            }
        },


        // Calculate some simple statistics, create text to place on plot
        calculateStatistics : function () {

            var debug = false, countYear = 0;

            // Empty current annotations
            DATA_DISPLAY.plotData.annotations["0"] = [];
            DATA_DISPLAY.plotData.annotations["1"] = [];
            DATA_DISPLAY.plotData.annotations["2"] = [];

            // Loop over years
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {

                // Create a separate annotation for each year
                var sum = 0, total = 0, avg = 0, i, hours, minutes, avgString,
                    yPlacement = {},
                    distanceText = "", newAnnotation = {}, numPartArry = [],
                    countDist = 0, totalParticipants = 0, totalNumBrevet = 0,
                    minParticipants = 1000, maxParticipants = 0;

                // Only continue if this year is being plotted
                if (DATA_DISPLAY.plotBoolean.year[year]) {

                    numPartArry = DATA_DISPLAY.plotData.numParticpants[year];

                    // Loop over number of participants for data being plotted,
                    // sum them
                    for (i = 0; i < numPartArry.length; i += 1) {

                        // Watch out for bad values - should do this during
                        // initial loading of data!
                        if (isNaN(numPartArry[i])) {
                            console.log("i: ", i, "  ", numPartArry[i]);
                        } else {
                            totalParticipants += numPartArry[i];
                            if (numPartArry[i] < minParticipants) {
                                minParticipants = numPartArry[i];
                            }
                            if (numPartArry[i] > maxParticipants) {
                                maxParticipants = numPartArry[i];
                            }
                        }
                    }

                    // Calculate simple averages
                    totalNumBrevet = numPartArry.length;
                    // avgParticipants = sumParticipants / totalNumBrevet;

                    // Loop over times for data being plotted, sum them
                    for (i = 0;
                            i < DATA_DISPLAY.plotData.xScatter[year].length;
                            i += 1) {

                        // Watch out for bad values - should do this during
                        // initial loading of data!
                        if (isNaN(DATA_DISPLAY.plotData.xScatter[year][i])) {
                            console.log("i: ", i, "  ",
                                DATA_DISPLAY.plotData.xScatter[year][i]);
                        } else {
                            sum += DATA_DISPLAY.plotData.xScatter[year][i];
                        }
                    }

                    // Calculate simple averages
                    total = DATA_DISPLAY.plotData.xScatter[year].length;
                    avg = sum / total;

                    // Convert the average to HH:MM format
                    hours = Math.floor(Math.abs(avg));
                    minutes = Math.floor((Math.abs(avg) * 60) % 60);
                    avgString = (hours < 10 ? "0" : "") + hours + ":" +
                        (minutes < 10 ? "0" : "") + minutes;

                    if (debug) {
                        console.log("Year: ", year);
                        console.log("Total Number: ", total);
                        console.log("Average: ", avg);
                        console.log("Average: ", avgString);
                        console.log("color: ",
                            DATA_DISPLAY.plotSettings.color[year]);
                        console.log("countYear: ", countYear);
                    }

                    // Create some text about distances being displayed
                    // Loop over distance values
                    Object.keys(DATA_DISPLAY.plotBoolean.distance).forEach(
                        function (distance) {
                            if (DATA_DISPLAY.plotBoolean.distance[distance] &&
                                    distance !== "ALL") {
                                if (countDist === 0) {
                                    distanceText = distance;
                                } else {
                                    distanceText = distance + ", " +
                                        distanceText;
                                }
                                countDist += 1;
                            }
                        }
                    );

                    if (countDist > 1) {
                        distanceText = "Distanser: " + distanceText;
                    } else {
                        distanceText = "Distans: " + distanceText;
                    }


                    // Create an annotation for this year's distribution
                    if (i > 0) {
                        yPlacement["1"] = 0.1 + 0.22 * countYear;
                        yPlacement["2"] = 0.1 + 0.4 * countYear;

                        if (debug) {
                            console.log("yPlacement[1]: ", yPlacement["1"]);
                            console.log("yPlacement[2]: ", yPlacement["2"]);
                            console.log("");
                        }

                        newAnnotation["1"] =
                            {
                                "text" : "År: " + year + " <br>" +
                                    distanceText + " <br>" +
                                    "Total Lopp: " + total +
                                    "<br>Genomsnittstid : " + avgString,
                                "x" : 1.0,
                                "y" : yPlacement["1"],
                                "yref" : 'paper',
                                "xref" : 'paper',
                                "showarrow" : false,
                                "font" : {
                                    "color" :
                                        DATA_DISPLAY.plotSettings.color[year],
                                    "size" : 12
                                },
                                "align": "left",
                                "borderpad" : 4,
                                "bgcolor" : "#181817",
                                "opacity" : 0.7
                            };

                        newAnnotation["2"] =
                            {
                                "text" : "År: " + year + " <br>" +
                                    "Min Deltagare: " + minParticipants +
                                     " <br>" +
                                    "Max deltagare: " + maxParticipants +
                                     " <br>" +
                                    "Total deltagare: " + totalParticipants +
                                     " <br>" +
                                    "Total Brevet: " + totalNumBrevet,
                                "x" : 1.0,
                                "y" : yPlacement["2"],
                                "yref" : 'paper',
                                "xref" : 'paper',
                                "showarrow" : false,
                                "font" : {
                                    "color" :
                                        DATA_DISPLAY.plotSettings.color[year],
                                    "size" : 12
                                },
                                "align": "left",
                                "borderpad" : 4,
                                "bgcolor" : "#181817",
                                "opacity" : 0.7
                            };

                        DATA_DISPLAY.plotData.annotations["1"].push(
                            newAnnotation["1"]
                        );

                        DATA_DISPLAY.plotData.annotations["2"].push(
                            newAnnotation["2"]
                        );

                        countYear += 1;
                    }
                }
            });

        },


        // Create option lists based on what has been read from csv files
        populateOptionsLists : function () {

            var debug = false, i, brevetObject, startPlaceList = [],
                startDateList = [], distanceList = [], yearList = [],
                divName, newHTML;

            if (debug) {
                console.debug('***  populateOptionsLists called  ***');
                console.log(DATA_LOAD.brevetObjectArray);
                console.log(DATA_LOAD.brevetObjectArray["2021"].length);
                console.log(DATA_LOAD.brevetObjectArray["2020"].length);
            }

            // Get unique list of start places, distances, years, dates
            // First - loop over each year
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {

                DATA_DISPLAY.startDateList[year] = [];

                // Second - loop over each brevet item
                for (i = 0; i < DATA_LOAD.brevetObjectArray[year].length;
                        i += 1) {

                    // Get the brevet object from the array
                    brevetObject = DATA_LOAD.brevetObjectArray[year][i];

                    // year
                    if (!yearList.includes(brevetObject.year)) {
                        yearList.push(brevetObject.year);
                    }

                    // distance
                    if (!distanceList.includes(brevetObject.distance)) {
                        distanceList.push(brevetObject.distance);
                    }

                    // place
                    if (!startPlaceList.includes(brevetObject.place)) {
                        startPlaceList.push(brevetObject.place);
                    }

                    // date
                    if (!startDateList.includes(brevetObject.date)) {
                        startDateList.push(brevetObject.date);
                        DATA_DISPLAY.startDateList[year].push(
                            brevetObject.date
                        );
                    }

                }
            });

            if (debug) {
                console.log("done getting unique lists");
            }

            // Sort lists alphabetically or numerically, some in reverse
            yearList = yearList.sort(function (a, b) {return b - a; });
            distanceList = distanceList.sort(function (a, b) {return b - a; });
            startPlaceList = startPlaceList.sort();
            startDateList = startDateList.sort().reverse();
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {
                DATA_DISPLAY.startDateList[year] =
                    DATA_DISPLAY.startDateList[year].sort().reverse();
            });

            if (debug) {
                console.log("done sorting lists");
                console.log("now  adding year options");
            }

            for (i = 0; i < yearList.length; i += 1) {

                // Add to global variable, set default visibility
                if (yearList[i] === "2021" || yearList[i] === "2020") {
                    DATA_DISPLAY.plotBoolean.year[yearList[i]] = true;
                } else {
                    DATA_DISPLAY.plotBoolean.year[yearList[i]] = false;
                }

                // Add to the html
                newHTML =
                    "<a id='plotOptionyear" + yearList[i] + "'" +
                    "onclick='DATA_DISPLAY.toggleItem(\"year\", \"" +
                    yearList[i] + "\")'>" +
                    "<i class='fa fa-check' ";
                if (DATA_DISPLAY.plotBoolean.year[yearList[i]]) {
                    newHTML += "style='visibility:visible'";
                } else {
                    newHTML += "style='visibility:hidden'";
                }
                newHTML += ">" + "</i>" +
                    "<span style='color:" +
                    DATA_DISPLAY.plotSettings.color[yearList[i]] + ";'>" +
                    yearList[i] + "</span>" + "</a>";
                if (i < 6) {
                    divName = 'plotOptionyearLeft';
                } else if (i < 12) {
                    divName = 'plotOptionyearMiddle';
                } else {
                    divName = 'plotOptionyearRight';
                }
                document.getElementById(divName).innerHTML += newHTML;
            }

            if (debug) {
                console.log("done adding year options");
                console.log("now  adding distance options");
            }

            for (i = 0; i < distanceList.length; i += 1) {

                // Add to global variable, set default visibility
                if (distanceList[i] === "200") {
                    DATA_DISPLAY.plotBoolean.distance[distanceList[i]] = true;
                } else {
                    DATA_DISPLAY.plotBoolean.distance[distanceList[i]] = false;
                }

                // Add to the html
                newHTML =
                    "<a id='plotOptiondistance" + distanceList[i] + "'" +
                    "onclick='DATA_DISPLAY.toggleItem(\"distance\", \"" +
                    distanceList[i] + "\")'>" +
                    "<i class='fa fa-check' ";
                if (DATA_DISPLAY.plotBoolean.distance[distanceList[i]]) {
                    newHTML += "style='visibility:visible'";
                } else {
                    newHTML += "style='visibility:hidden'";
                }
                newHTML += ">&nbsp;" + "</i>" + distanceList[i] + "</a>";
                if (i < 3) {
                    divName = 'plotOptiondistanceLeft';
                } else if (i < 6) {
                    divName = 'plotOptiondistanceMiddle';
                } else {
                    divName = 'plotOptiondistanceRight';
                }
                document.getElementById(divName).innerHTML += newHTML;
            }

            if (debug) {
                console.log("done adding distance options");
                console.log("now  adding place options");
            }

            for (i = 0; i < startPlaceList.length; i += 1) {

                // Add to global variable, set default visibility
                DATA_DISPLAY.plotBoolean.place[startPlaceList[i]] = true;

                // Add to the html
                newHTML =
                    "<a id='plotOptionplace" + startPlaceList[i] + "'" +
                    "onclick='DATA_DISPLAY.toggleItem(\"place\", \"" +
                    startPlaceList[i] + "\")'>" +
                    "<i class='fa fa-check' " +
                    "style='visibility:visible'>&nbsp;" +
                    "</i>" + startPlaceList[i] + "</a>";
                document.getElementById('plotOptionplace').innerHTML +=
                    newHTML;
            }

            if (debug) {
                console.log("done adding place options");
                console.log("now  adding date options");
            }

            for (i = 0; i < yearList.length; i += 1) {
                if (DATA_DISPLAY.plotBoolean.year[yearList[i]]) {
                    DATA_DISPLAY.populateHTMLDateList(yearList[i]);
                }
            }

            if (debug) {
                console.log("done adding date options");
            }

            if (debug) {
                console.log(yearList);
                console.log(DATA_DISPLAY.plotBoolean.year);
                console.log(distanceList);
                console.log(DATA_DISPLAY.plotBoolean.distance);
                console.log(startPlaceList);
                console.log(startDateList);
                console.log(DATA_DISPLAY.plotBoolean.place);

                console.debug('***  populateOptionsLists done  ***');
            }

        },


        populateHTMLDateList : function (year) {

            var debug = false, j, dateOption, elementID, newHTML;

            if (debug) {
                console.log("year:" + year);
                console.log("DATA_DISPLAY.plotBoolean.year[" + year + "]:" +
                    DATA_DISPLAY.plotBoolean.year[year]);
                console.debug("DATA_DISPLAY.startDateList[" + year +
                    "].length:" + DATA_DISPLAY.startDateList[year].length);
            }

            for (j = 0; j <
                    DATA_DISPLAY.startDateList[year].length; j += 1) {

                dateOption = DATA_DISPLAY.startDateList[year][j];

                // Only add to html if it has not yet been done
                elementID = "plotOptiondate" + dateOption;
                if ($("#" + elementID).length === 0) {

                    // Add to the html
                    newHTML =
                        "<a id='" + elementID + "' ";
                    newHTML += "onclick='DATA_DISPLAY.toggleItem(\"date\", \""
                        + dateOption + "\")'>" + "<i class='fa fa-check' " +
                        "style='visibility:visible'>&nbsp;" +
                        "</i>" + dateOption + "</a>";

                    document.getElementById('plotOptiondate').innerHTML +=
                        newHTML;
                } else {
                    if (debug) {
                        console.log("element exists: " + elementID);
                        console.log("DATA_DISPLAY.plotBoolean.year[year]: " +
                            DATA_DISPLAY.plotBoolean.year[year]);
                    }

                    // Remove the item from the html
                    if (!DATA_DISPLAY.plotBoolean.year[year]) {
                        document.getElementById('plotOptiondate').removeChild(
                            document.getElementById(elementID)
                        );
                    }
                }

                // Add to global variable, set visibility
                DATA_DISPLAY.plotBoolean.date[dateOption] =
                    DATA_DISPLAY.plotBoolean.year[year];
            }

        },


        // Filter data for plotting based upon what has been selected
        filterData : function () {

            var debug = false, i, brevetObject, repeatedPoints = {},
                timeHours, distance, hoverText, opacity, markerSize, timeKey,
                distKey, countParticipants = {};

            if (debug) {
                console.log('filterData called');
            }

            // Loop over years
            Object.keys(DATA_LOAD.brevetObjectArray).forEach(function (year) {

                // Empty plotting arrays
                Object.keys(DATA_DISPLAY.plotData).forEach(
                    function (plotItem) {
                        DATA_DISPLAY.plotData[plotItem][year] = [];
                    }
                );

                // Only continue if this year is being plotted
                if (DATA_DISPLAY.plotBoolean.year[year]) {

                    repeatedPoints = {};
                    countParticipants = {};

                    // Loop over each brevet item in the complete data list
                    for (i = 0; i < DATA_LOAD.brevetObjectArray[year].length;
                            i += 1) {

                        // Get the brevet object from the array
                        brevetObject = DATA_LOAD.brevetObjectArray[year][i];

                        // See if this brevet object satisifies the criteria
                        if (DATA_DISPLAY.keepObject(brevetObject)) {

                            // Check for repeated points, alter some the items
                            // accordingly
                            repeatedPoints =
                                DATA_DISPLAY.examineRepeatedPoints(
                                    brevetObject.timeHours,
                                    brevetObject.distance,
                                    brevetObject.firstname,
                                    brevetObject.lastname,
                                    brevetObject.club,
                                    brevetObject.place,
                                    brevetObject.date,
                                    brevetObject.time,
                                    repeatedPoints
                                );

                            // Keep a running total of the number of partipants
                            // in each brevet, which will have unique:
                            //  - date
                            //  - place
                            //  - distance
                            countParticipants =
                                DATA_DISPLAY.countParticipants(
                                    brevetObject.date,
                                    brevetObject.place,
                                    brevetObject.distance,
                                    brevetObject.numPeople,
                                    countParticipants
                                );

                            // Get altered items in repeatedPoints
                            timeHours = brevetObject.timeHours;
                            distance = brevetObject.distance;
                            hoverText =
                                repeatedPoints[timeHours][distance].hoverText;
                            opacity =
                                repeatedPoints[timeHours][distance].opacity;
                            markerSize =
                                repeatedPoints[timeHours][distance].markerSize;

                            // Now fill all of the data plotting arrays
                            DATA_DISPLAY.plotData.xScatter[year].push(
                                timeHours
                            );
                            DATA_DISPLAY.plotData.yScatter[year].push(
                                distance
                            );
                            DATA_DISPLAY.plotData.xHist[year].push(
                                timeHours
                            );
                            DATA_DISPLAY.plotData.hoverText[year].push(
                                hoverText
                            );
                            DATA_DISPLAY.plotData.opacity[year].push(
                                opacity
                            );
                            DATA_DISPLAY.plotData.markerSize[year].push(
                                markerSize
                            );

                        }

                    }

                    // Look through the object array, reset hover text and
                    // marker sizes for repeated points (same distance and
                    // time)
                    for (i = 0;
                            i < DATA_DISPLAY.plotData.xScatter[year].length;
                            i += 1) {

                        // Get the brevet object from the array
                        timeKey = String(
                            DATA_DISPLAY.plotData.xScatter[year][i]
                        );
                        distKey = String(
                            DATA_DISPLAY.plotData.yScatter[year][i]
                        );

                        // Check if this point has been repeated
                        if (repeatedPoints.hasOwnProperty(timeKey)) {
                            if (repeatedPoints[timeKey].hasOwnProperty(
                                    distKey
                                )) {

                                // Reset the values of the hover text, marker
                                // size, and opacity
                                DATA_DISPLAY.plotData.hoverText[year][i] =
                                    repeatedPoints[timeKey][distKey].hoverText;
                                DATA_DISPLAY.plotData.opacity[year][i] =
                                    repeatedPoints[timeKey][distKey].opacity;
                                DATA_DISPLAY.plotData.markerSize[year][i] =
                                    repeatedPoints[timeKey][
                                        distKey
                                    ].markerSize;
                            }
                        }
                    }

                    if (debug) {
                        console.log(countParticipants);
                    }

                    // Place counts of partipants into an array to be used for
                    // plotting
                    Object.keys(countParticipants).forEach(function (date) {
                        Object.keys(countParticipants[date]).forEach(
                            function (place) {
                                Object.keys(
                                    countParticipants[date][place]
                                ).forEach(function (distance) {

                                    DATA_DISPLAY.plotData.numParticpants[
                                        year
                                    ].push(
                                        countParticipants[date][place][
                                            distance
                                        ]
                                    );

                                });
                            }
                        );
                    });

                }

            });

            if (debug) {
                console.log(DATA_DISPLAY.plotData.xScatter["2020"][0]);
                console.log(DATA_DISPLAY.plotData.yScatter["2020"][0]);
                console.log(DATA_DISPLAY.plotData.xHist["2020"][0]);
                console.log(DATA_DISPLAY.plotData.numParticpants["2020"][0]);
                console.log(DATA_DISPLAY.plotData.hoverText["2020"][0]);

                console.log("Done with filterData");
            }

        },


        // Decide whether or not to plot a brevet object based upon which items
        // have been selected
        keepObject : function (brevetObject) {

            var debug = false, keepThisOne = true,
                plotBoolean = DATA_DISPLAY.plotBoolean, plotBooleanValues,
                brevetItemValue;

            // Loop over the items in this brevet object (e.g. "distance",
            // "place", "club", etc.)
            Object.keys(brevetObject).forEach(
                function (brevetItem) {

                    // Check if the visibility of this brevet object item has
                    // been specified ("year", "distance", "place", "date")
                    if (plotBoolean.hasOwnProperty(brevetItem)) {

                        // Get a list of possible values for this item, e.g.
                        // for the "distance" item the values are 200, 300,
                        // etc.
                        plotBooleanValues = plotBoolean[brevetItem];

                        // Get the value for this item in the brevet object,
                        // e.g. 200 if the item is "distance"
                        brevetItemValue = brevetObject[brevetItem];

                        // Check if the visibility of this value of the item
                        // has been specified, e.g. 200
                        if (plotBooleanValues.hasOwnProperty(
                                brevetItemValue
                            )) {

                            if (debug) {
                                console.log(brevetItem + " -> " +
                                    brevetItemValue);
                                console.log(" -> " +
                                    plotBooleanValues[brevetItemValue]);
                            }

                            // This will be true or false
                            if (keepThisOne) {
                                keepThisOne =
                                    plotBooleanValues[brevetItemValue];
                            }
                            if (debug) {
                                console.log(" -> keepThisOne: ", keepThisOne);
                            }
                        }
                    }
                }
            );

            // Now check if the distance and time are with in the selected
            // zoom range
            if (keepThisOne) {
                if (brevetObject.timeHours <
                            DATA_DISPLAY.zoomRange.selected.time[0] ||
                        brevetObject.timeHours >
                            DATA_DISPLAY.zoomRange.selected.time[1] ||

                        brevetObject.distance <
                            DATA_DISPLAY.zoomRange.selected.distance[0] ||
                        brevetObject.distance >
                            DATA_DISPLAY.zoomRange.selected.distance[1] ||

                        brevetObject.numPeople <
                            DATA_DISPLAY.zoomRange.selected.numPeople[0] ||
                        brevetObject.numPeople >
                            DATA_DISPLAY.zoomRange.selected.numPeople[1]
                        ) {

                    keepThisOne = false;

                    if (debug) {
                        console.log("Dropping this one, outside zoom region:");
                        console.log(brevetObject);
                    }
                }
            }

            if (debug) {
                console.log("* keepThisOne: ", keepThisOne);
                console.log("");
            }

            return keepThisOne;
        },

        // Look for repeated points - those with the same distance and time
        examineRepeatedPoints : function (timeHours, distance, firstname,
            lastname, club, place, date, time, repeatedPoints) {

            var debug = false, markerSize, count, opacity, hoverText,
                timeKey, distKey, createNewEntry = false;

            // Combine text and increase marker size for multiple occurrances
            // of the same time and distance
            timeKey = String(timeHours);
            distKey = String(distance);

            // Check if a brevet item for the same time and distance exists
            if (repeatedPoints.hasOwnProperty(timeKey)) {
                if (repeatedPoints[timeKey].hasOwnProperty(distKey)) {

                    // Increase the marker size and reduce the opacity, which
                    // in the end will result in the same opacity as points
                    // will overlap and add together.
                    markerSize =
                        repeatedPoints[timeKey][distKey].markerSize + 2;
                    count = repeatedPoints[timeKey][distKey].count + 1;
                    opacity = 0.7 / count;

                    // Start with the existing hover text
                    hoverText =
                        repeatedPoints[timeKey][distKey].hoverText;

                } else {
                    createNewEntry = true;
                }
            } else {
                createNewEntry = true;
                repeatedPoints[timeKey] = {};
            }

            // The default values for a single point
            if (createNewEntry) {
                markerSize = 7;
                count = 1;
                opacity = 0.7;
                hoverText = String('<b>' + time + '</b>');
            }

            // Append to the hover text
            hoverText = hoverText.concat('<br><b> ',
                firstname, ' ', lastname, '</b> - <i>', club,
                '</i> - ', place, ' ', date);

            // Set the items in this object array
            repeatedPoints[timeKey][distKey] =
                {
                    "markerSize" : markerSize,
                    "count" : count,
                    "opacity" : opacity,
                    "hoverText" : hoverText,
                };

            if (debug) {
                console.log("repeat: ", timeKey, ", ",
                    distKey, ", ",
                    repeatedPoints[timeKey][distKey]);
            }

            // Return the object array to be used again
            return repeatedPoints;
        },


        // Keep a running total of the numebr of partipants
        // in each brevet, which will have unique:
        //  - date
        //  - place
        //  - distance
        countParticipants : function (date, place, distance, numPeople,
            countParticipants) {

            var debug = false;

            if (debug) {
                console.log('date:            ' + date);
                console.log('place:           ' + place);
                console.log('distance:        ' + distance);
                console.log('numPeople: ' + numPeople);
                console.log(countParticipants);
            }

            if (countParticipants.hasOwnProperty(date)) {
                if (countParticipants[date].hasOwnProperty(place)) {
                    if (countParticipants[date][place].hasOwnProperty(
                            distance
                        )) {
                        // countParticipants[date][place][distance] += 1;
                        countParticipants[date][place][distance] =
                            numPeople;
                    } else {
                        // countParticipants[date][place][distance] = 1;
                        countParticipants[date][place][distance] =
                            numPeople;
                    }
                } else {
                    countParticipants[date][place] = {};
                    // countParticipants[date][place][distance] = 1;
                    countParticipants[date][place][distance] =
                        numPeople;
                }
            } else {
                countParticipants[date] = {};
                countParticipants[date][place] = {};
                // countParticipants[date][place][distance] = 1;
                countParticipants[date][place][distance] =
                    numPeople;
            }

            return countParticipants;
        },


        // Handle a 2D image.  A new image is always fetched, even if the
        // presently displayed image has not been decimated, as it makes
        // several other operations simpler.
        handleZoom : function (traceArrayNumber, eventdata) {

            var debug = false, result, ranges = [-1, -1, -1, -1], reset;

            if (debug) {
                console.debug('*** START DATA_DISPLAY.handleZoom ***');
                console.debug("traceArrayNumber: " + traceArrayNumber);
                console.debug(eventdata);
            }

            // Check if this is a zoom event
            if (DATA_DISPLAY.isZoomEvent(eventdata)) {

                // Get the zoom range
                result = DATA_DISPLAY.getZoomRange(eventdata);
                ranges = result.ranges;
                reset = result.reset;

                // Save the zoom/section information
                if (traceArrayNumber === "0") {
                    DATA_DISPLAY.zoomRange.selected.time =
                        [ranges[0], ranges[1]];
                    DATA_DISPLAY.zoomRange.selected.distance =
                        [ranges[2], ranges[3]];
                }

                if (traceArrayNumber === "1") {
                    DATA_DISPLAY.zoomRange.selected.time =
                        [ranges[0], ranges[1]];
                }

                if (traceArrayNumber === "2") {
                    DATA_DISPLAY.zoomRange.selected.numPeople =
                        [ranges[0], ranges[1]];
                }

                if (reset) {
                    DATA_DISPLAY.zoomRange.selected.time =
                        DATA_DISPLAY.zoomRange.default.time;
                    DATA_DISPLAY.zoomRange.selected.distance =
                        DATA_DISPLAY.zoomRange.default.distance;
                    DATA_DISPLAY.zoomRange.selected.numPeople =
                        DATA_DISPLAY.zoomRange.default.numPeople;
                }

                if (debug) {
                    console.log("Saved zoom ranges:");
                    console.log("  DATA_DISPLAY.zoomRange.selected.time:     "
                        + DATA_DISPLAY.zoomRange.selected.time);
                    console.log("  DATA_DISPLAY.zoomRange.selected.distance: "
                        + DATA_DISPLAY.zoomRange.selected.distance);
                    console.log("  DATA_DISPLAY.zoomRange.selected.numPeople: "
                        + DATA_DISPLAY.zoomRange.selected.numPeople);
                }

                DATA_DISPLAY.reDrawPlots();
            }

            if (debug) {
                console.debug('***  END  DATA_DISPLAY.handleZoom ***');
            }
        },


        // Check if an event is a zoom event
        // Similar event signals are triggered for browser window resizing
        isZoomEvent : function (eventdata) {

            var debug = false, i = 0, zoomEvent = false,
                rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                    'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'];

            if (debug) {
                console.log(JSON.stringify(eventdata));
            }

            // Zoom events return json objects containing keys like
            // 'xaxis.range' or 'xaxis.autorange'
            for (i = 0; i < rangeKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(rangeKeys[i])) {
                    zoomEvent = true;
                }
            }

            for (i = 0; i < autoKeys.length; i += 1) {
                if (eventdata.hasOwnProperty(autoKeys[i])) {
                    zoomEvent = true;
                }
            }

            if (debug) {
                if (!zoomEvent) {
                    console.log('** DATA_DISPLAY.isZoomEvent Does not Look' +
                        ' like a zoom event');
                }
            }

            return zoomEvent;
        },


        getZoomRange : function (eventdata) {

            var debug = false, i = 0,
                ranges = [0, 300, 0, 3000, 0, 300],
                maxRanges = [0, 300, 0, 3000, 0, 300],
                rangeKeys = ['xaxis.range[0]', 'xaxis.range[1]',
                    'yaxis.range[0]', 'yaxis.range[1]'],
                autoKeys = ['xaxis.autorange', 'yaxis.autorange'],
                reset = false;

            // Loop over the 4 range values - x & y, min & max
            for (i = 0; i < ranges.length; i += 1) {

                // Look at the 'range' keys, set ranges
                if (eventdata.hasOwnProperty(rangeKeys[i])) {

                    ranges[i] = eventdata[rangeKeys[i]];

                    if (debug) {
                        console.log(rangeKeys[i]);
                        console.log(eventdata[rangeKeys[i]]);
                    }

                }

                // Make sure we haven't gone too far
                if (DATA_DISPLAY.isEven(i)) {
                    if (ranges[i] < maxRanges[i]) {
                        ranges[i] = maxRanges[i];
                    }
                } else {
                    if (ranges[1] > maxRanges[1]) {
                        ranges[1] = maxRanges[1];
                    }
                }

            }

            // Check for reset-zoom events
            if (eventdata.hasOwnProperty(autoKeys[0]) ||
                    eventdata.hasOwnProperty(autoKeys[1])) {
                for (i = 0; i < ranges.length; i += 1) {
                    ranges[i] = maxRanges[i];
                }
                reset = true;
            }

            if (debug) {
                console.log('x-axis start: ' + ranges[0]);
                console.log('x-axis end:   ' + ranges[1]);
                console.log('y-axis start: ' + ranges[2]);
                console.log('y-axis end:   ' + ranges[3]);
            }

            return {
                ranges : ranges,
                reset : reset,
            };
        },


        // Check if odd or even
        isEven : function (n) {
            var result = (n % 2 === 0) ? true : false;
            return result;
        },

    };


// This function fires when the page is ready
$(document).ready(function () {

    var debug = false;

    if (debug) {
        console.log('document is ready');
    }
});
