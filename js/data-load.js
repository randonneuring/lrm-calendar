/*global $*/
'use strict';


// External libraries
var DATA_DISPLAY,

    // Global variables
    DATA_LOAD =
        {
            // The relative location of the data files
            dataSource : {
                "2022" : "data/lrm-calendar-2022.json"
            },

            // The dictionary array into which all loaded data will be placed
            brevetObjectArray : {},
            response : [],

            // Send an ajax request
            ajaxRequest : function (url, dataType) {

                var debug = false;

                // Send an ajax request to the server - the return value is the
                // response
                return $.ajax({

                    // Add version number url
                    url: url + "?v=202110271541",

                    success: function (response) {
                        if (debug) {
                            console.debug("AJAX " + url + " request success");
                            console.debug(response);
                        }
                    },

                    error: function (x, status, error) {
                        // Unauthorized access error
                        if (x.status === 403) {
                            console.log(
                                'HTTP 403: Forbidden (Access is not permitted)'
                            );
                        // Other errors
                        } else {
                            console.log("An error occurred: " + status);
                            console.log("Error: " + error);
                        }
                    },

                    // Settings used in all ajax requests
                    type: "GET",
                    dataType: dataType,
                    async: true,
                    cache: true,
                    timeout: 20000
                });

            },


            // The id of the table needs to already exist in the html file
            // The column names need to match what is in the data files
            createTable : function (data) {
                $('#calendar_table').DataTable(
                    {
                        "data": data,
                        "columns": [
                            { "data": "Date" },
                            { "data": "Country" },
                            { "data": "Location" },
                            { "data": "Distance" },
                            { "data": "Name" },
                            { "data": "Organizer" },
                        ],
                        "paging": false,
                        "select": true,
                        "dom": 'Bfrtip',
                        "buttons": [
                            'print',
                            'pdf',
                        ],
                        "fixedHeader": {
                            "header": true,
                            "footer": true
                        }
                    }
                ).columns.adjust();
                // The "columns.adjust()" is executed inorder to force a
                // re-draw of the table, otherwise the table does not properly
                // fill the available space properly during a resize event
            },


            // Load the data from given json files
            loadBrevetData : function () {

                var debug = false, dataFile, requests = [];

                if (debug) {
                    console.log("loadBrevetData called");
                }

                // Loop over each year and read the data file associated with
                // it
                Object.keys(DATA_LOAD.dataSource).forEach(
                    function (year, index) {

                        // Get the data file name
                        dataFile = DATA_LOAD.dataSource[year];

                        if (debug) {
                            console.log("year: ", year, ", index: ",
                                index);
                            console.log("dataFile: ", dataFile);
                        }

                        // Read json data, run as ajax request.
                        requests.push(
                            $.when(DATA_LOAD.ajaxRequest(dataFile,
                                "json")).then(

                                function (response) {
                                    // Save output to a global variable
                                    DATA_LOAD.response[year] = response;
                                }
                            )
                        );

                    }
                );


                // After data reading is done, send to table creation function
                $.when.apply($, requests).then(
                    function () {
                        var i, name, url, link, organizer, email, mailto;

                        if (debug) {
                            console.log(DATA_LOAD.response["2022"][0]);
                        }


                        // Loop over all data elements
                        for (i = 0; i < DATA_LOAD.response["2022"].length;
                                i += 1) {

                            // Create html link from url and name
                            name = DATA_LOAD.response["2022"][i].Name;
                            url = DATA_LOAD.response["2022"][i].Url;
                            link = '<a target="_blank" rel="noreferrer"' +
                                ' href="' + url + '">' +
                                name + '</a>';

                            // Create mailto link from email and organizer
                            organizer =
                                DATA_LOAD.response["2022"][i].Organizer;
                            email = DATA_LOAD.response["2022"][i].Email;
                            mailto = '<a target="_blank" rel="noreferrer"' +
                                ' href="mailto:' + email + '">' +
                                organizer + '</a>';

                            if (debug) {
                                if (i < 5) {
                                    console.log(name);
                                    console.log(url);
                                    console.log(link);
                                    console.log(organizer);
                                    console.log(email);
                                    console.log(mailto);
                                }
                            }

                            // Overwrote values for Name and Organizer
                            DATA_LOAD.response["2022"][i].Name = link;
                            DATA_LOAD.response["2022"][i].Organizer = mailto;
                        }

                        // Create the table
                        DATA_LOAD.createTable(DATA_LOAD.response["2022"]);
                    }
                );

            },

        };


// This function fires when the page is ready
$(document).ready(function () {

    var debug = true;

    if (debug) {
        console.log('document is ready');
    }

    // Load all the data and plot it
    DATA_LOAD.loadBrevetData();

});
