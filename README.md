# LRM (LES RANDONNEURS MONDIAUX)

This is a web application for displaying and searching through a list of long
distance brevet rides,

A live version is currently available here:
[LRM Calendar 2022](https://randonneuring.gitlab.io/lrm-calendar/)

The source for the brevet schedule themselves has been taken from the official
LRM spreadsheet:
[LRM Spreadsheet](https://docs.google.com/spreadsheets/d/e/2PACX-1vTVPNLvQkJDKwO4axHHy4P7DnibG9bd8IA-bvz31ah_N1BZogM7Wo3CMrDhT3FyIdadJfltwcqjEpn3/pubhtml?gid=1480200001&single=true).

Screenshot:
![screenshot](screenshots/2021-10-26_screenshot.png)
